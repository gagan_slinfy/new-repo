      <button type="button" class="marketing-nav__hamburger hide--desktop js-drawer-open-right" aria-controls="NavDrawer" aria-expanded="false">
        <svg class="icon" role="img" aria-labelledby="hamburger">
          <title id="hamburger">Open main navigation</title>
          <svg xmlns="#" viewBox="0 0 88 88"><path fill="#FFF" d="M0 12.3h88.4v9H0zm0 29.2h88.4v9H0zm0 29.1h88.4v9H0z"/></svg>
        </svg>
      </button>